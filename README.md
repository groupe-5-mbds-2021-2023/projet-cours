Lecoincoin est une application pour gérer des annonces

Back Office:

Pagination de toutes les listes

- Gestion des utilisateurs
    - Liste des utilisateurs
    - Modification des utilisateurs
    - Création d'un nouvel utilisateur
    - Suppresion d'un utilisateur

- Gestion des annonces
    - Liste des annonces
    - Modification d'une annonce
    - Création d'une nouvelle annonce
    - Suppresion d'une annonce

- Login
    - Gestion des rôles et permissions des utilisateurs:
        - Admin: création, modification, suppression d'une annonce ou d'un utilisateur
        - Modérateur: modification d'une annonce ou d'un utilisateur
        - Client: peut seulement voir la liste des utilisateurs ou des annonces

        Les pages de Lecoincoin ne sont pas visibles aux utilisateurs non authentifiés

- Recherche paginée d'une annonce

API Rest

    - Liste des utilisateurs
    - Modification des utilisateurs
    - Création d'un nouvel utilisateur
    - Suppresion d'un utilisateur

    - Liste des annonces
    - Modification d'une annonce
    - Création d'une nouvelle annonce
    - Suppresion d'une annonce

    - Login

    Les données en réponse peuvent être données sous forme JSON ou XML

Collection Postman: https://www.getpostman.com/collections/60bcf173020a3a3f7f05


Installation:
- Cloner ce repository: https://gitlab.com/groupe-5-mbds-2021-2023/projet-cours.git
- Ouvrir le projet dans votre IDE (IntelliJ)
- Lancer Gradle tasks
- Lancer le projet
- Lecoincoin s'ouvrira dans le navigateur

