<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav ml-3" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="edit-annonce" class="container-fluid" role="main">
    <h3><g:message code="default.edit.label" args="[entityName]"/></h3><hr>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <g:uploadForm controller="annonce" action="update" id="${annonce.id}">
        <g:hiddenField name="version" value="${this.annonce?.version}"/>
        <fieldset class="form">
            <div class="row form-group">
                <div class="col-md-4">
                    <div class="mb-3 required">
                        <label for="title">Title
                            <span class="required-indicator">*</span>
                        </label>
                        <input class="form-control" type="text" name="title" value="${annonce.title}" required=""
                               id="title">
                    </div>

                    <div class="mb-3 required">
                        <label for="description">Description
                            <span class="required-indicator">*</span>
                        </label>
                        <input class="form-control" type="text" name="description" value="${annonce.description}"
                               required=""
                               id="description">
                    </div>

                    <div class="mb-3 required">
                        <label for="price">Price
                            <span class="required-indicator">*</span>
                        </label><input class="form-control" type="number decimal" name="price" value="${annonce.price}"
                                       required="" min="0.0"
                                       id="price">
                    </div>

                    <div class="mb-3 required">
                        <label for="author">Author
                            <span class="required-indicator">*</span>
                        </label>
                        <g:select class="form-control" name="author.id" from="${userList}" optionKey="id"
                                  optionValue="username"/>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-4">
                                <label class="mb-3" for="illustrations">Illustrations</label><br>
                                <g:each in="${annonce.illustrations}" var="illustration">
                                    <img class="mr-4" src="${baseUrl + illustration.filename}" alt="${illustration.filename}"/>
                                </g:each>
                            </div>

                            <div class="mb-3 ">
                                <label for="file">Upload</label>
                                <input class="form-control-file" style="display: inline" type="file" name="file"
                                       id="file"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset class="buttons">
            <input class="save" type="submit"
                   value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:uploadForm>
</div>
</body>
</html>
