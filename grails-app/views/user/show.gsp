<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-user" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label>Username :</label>

                            <p style="display:inline">${this.user.username}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label>Account expired :</label>

                            <p style="display:inline">${this.user.accountExpired}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label>Account locked :</label>

                            <p style="display:inline">${this.user.accountLocked}</p>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label>Password expired</label>
                            <p style="display:inline">${this.user.passwordExpired}</p>
                        </div>


                        <div class="col-md-12 mb-3">
                            <label>Enabled</label>
                            <p style="display:inline">${user.enabled}</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <label>Annonces :</label>
                    <ul>
                        <g:each in="${this.user.annonces}" var="annonce">
                            <li><a href="/projet/annonce/${annonce.id}">
                                ${annonce.title}
                            </a> </li>
                        </g:each>
                    </ul>
                </div>
            <g:form resource="${this.user}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.user}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
