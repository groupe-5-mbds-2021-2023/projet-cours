<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-user" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.user}" method="PUT">
                <g:hiddenField name="version" value="${this.user?.version}" />
                <fieldset class="form">
                    <div class="row form-group">
                        <div class="col-md-5">
                            <div class="mb-3 required">
                                <label for="username">Username
                                    <span class="required-indicator">*</span>
                                </label>
                                <input class="form-control" type="text" name="username" value="${user.username}"
                                       required=""
                                       id="description">
                            </div>

                            <div class="mb-3 required">
                                <label for="password">Password
                                    <span class="required-indicator">*</span>
                                </label>
                                <input class="form-control" type="password" name="password" value="${user.password}" required=""
                                       id="title">
                            </div>

                            <div class="mb-3 required">
                                <label for="passwordExpired">Password expired
                                </label>
                                <g:checkBox checked="${user.passwordExpired}" name="passwordExpired" value="${user.passwordExpired}"></g:checkBox>
                            </div>

                            <div class="mb-3 required">
                                <label for="accountLocked">Account locked
                                </label>
                                <g:checkBox checked="${user.accountLocked}" name="accountLocked" value="${user.accountLocked}"></g:checkBox>
                            </div>

                            <div class="mb-3 required">
                                <label for="accountExpired">Account expired
                                </label>
                                <g:checkBox checked="${user.accountExpired}" name="accountExpired" value="${user.accountExpired}"></g:checkBox>
                            </div>

                            <div class="mb-3 required">
                                <label for="enabled">Enabled
                                </label>
                                <g:checkBox name="enabled" checked="${user.enabled}" value="${user.enabled}"></g:checkBox>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <div class="mb-3 required">
                                <label for="annonce">Annonces <a href="/projet/annonce/create">Add annonce</a> </label>

                                <ul>
                                    <g:each in="${user.annonces}" var="annonce">
                                        <li><a href="/projet/annonce/${annonce.id}">
                                            ${annonce.title}
                                        </a> </li>
                                    </g:each>
                                </ul>

                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
